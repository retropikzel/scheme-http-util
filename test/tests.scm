(import (scheme base)
        (scheme write)
        (scheme file)
        (retropikzel http-util v0-1-0 main)
        (srfi 64))


(test-begin "headers->string")

(define headers
  '((Content-Type . "text/html")
    (User-Agent . "test")))

(define headers-string (http-util-headers->string headers))

(test-equal "Content-Type: text/html\nUser-Agent: test\n\n"
            headers-string)

(test-end "headers->string")


(test-begin "request-build")

(define request (http-util-request-build "GET"
                                         "gnu.org"
                                         headers
                                         ""))

(test-equal (string-append "GET gnu.org HTTP/1.1\n" headers-string "\n")
            request)

(test-end "request-build")


(test-begin "status-line->list")

(define statusline "HTTP/1.1 200 OK")
(test-equal '("HTTP/1.1" 200 "OK") (http-util-status-line->list statusline))

(test-end "status-line->list")


(test-begin "header-line->pair")

(define headerline "Content-Type: text/html")

(test-equal '(content-type . "text/html") (http-util-header-line->pair headerline))

(test-end "header-line->pair")


(test-begin "read-http-response")

(define test-response "HTTP/1.1 200 OK\nContent-Type: text/html\nContent-Length: 86\n\n<html>\n<body>\n<h1>Hello world</h1>\n</body>\n</html>")

(define response (http-util-read-http-response (open-input-string test-response)))
(write response)

(test-end "read-http-response")

(test-begin "request-make")

(define html "<html>\n    <head></head>\n    <body>\n        Hello from test HTML\n    </body>\n</html>\n")

(define response (http-util-request-make "GET" "localhost" "/test.html" 3000 '(( host . "test")) ""))

(display response)
(newline)

(test-equal 200 (car (cdr (assoc 'status-code response))))

(test-equal html (utf8->string (car (cdr (assoc 'body response)))))

(test-end "request-make")

(test-begin "download-file")

(define tmp-file "../.make/tmp-test.html")
(http-util-download-file "localhost" "/test.html" 3000 '() tmp-file)

(define file-content
  (with-input-from-file tmp-file
                        (lambda ()
                          (letrec ((looper
                                     (lambda (bytes result)
                                       (if (eof-object? bytes)
                                         result
                                         (looper (read-bytevector 4000) (bytevector-append result bytes))))))
                            (looper (read-bytevector 4000) (bytevector))))))

(test-equal (utf8->string file-content) html)

(test-end "download-file")
