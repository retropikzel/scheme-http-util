guix-shell:
	guix shell php gauche

.make:
	mkdir -p .make

run-tests: .make
	@bash test/run.sh

clean:
	rm *.log
	rm -rf .make
