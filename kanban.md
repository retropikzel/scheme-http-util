# Ideas

## Could this be made work with bytevectors?

Instead of reading directly from socket could this library take input as bytevectors? Then we would not need to depend on SRFI-106.

## Move URL encoding and decoding here
Currently that code is in scheme-cgi library. Make that library use this library for that functionality.

## http-util-request-make
If the path does not start with "/", then add it

# To do

# In progress

# Done
