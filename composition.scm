((packager . "retropikzel")
 (name . "http-util")
 (version . "v0.3.1")
 (type . "library")
 (license . "LGPL")
 (description . "Utilities relating to HTTP protocol")
 (ignore-files . ())
 (dependencies . ()))
